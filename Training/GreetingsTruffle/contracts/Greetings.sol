pragma solidity >=0.4.21 <0.6.0;

contract Greetings {
  string message;

  constructor() public {
    message = "I am Ready";
  }

  function setGreeting(string memory _message) public {
    message = _message;
  }

  function getGreeting() public view returns (string memory) {
    return message;
  }
}
